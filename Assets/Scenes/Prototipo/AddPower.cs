using UnityEngine;
using System.Collections;

public class AddPower : MonoBehaviour {
	public Globals.Powers selectedPower = Globals.Powers.Explosion;

	// Use this for initialization
	void Awake() {
		Globals.EnablePower(selectedPower);
	}
}
