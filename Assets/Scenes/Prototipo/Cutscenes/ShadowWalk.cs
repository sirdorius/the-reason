using UnityEngine;
using System.Collections;

public class ShadowWalk : MonoBehaviour {
	//public float speed = 120;
	public int minSpeed;
	public int maxSpeed;
	public bool randomDirection = false;
	
	private float speed;
	
	void Awake() {
		speed = Random.Range(minSpeed, maxSpeed);
		if (randomDirection && Random.value < 0.5f) speed *= -1;
	}
	
	// Update is called once per frame
	void Update () {
		float t = Time.deltaTime*Time.timeScale;
		transform.position += new Vector3(t*speed, 0,0);
		if (transform.position.x > 760f)
			transform.position += new Vector3(-1483f,0,0);
		else if(transform.position.x < -717f)
			transform.position += new Vector3(1483f,0,0);
	}
}
