using UnityEngine;
using UnityEditor;

public class PathTool : EditorWindow {
	private PathMovement p;

	// Use this for initialization
	[MenuItem("Window/Path Tool")]
	static void Init () {
        // Get existing open window or if none, make a new one:
        EditorWindow.GetWindow (typeof (PathTool));
    }
	
	void OnGUI () {
		GUILayout.BeginHorizontal();
		GUILayout.Label("Seleziona un oggetto che possiede PathMovement", EditorStyles.boldLabel);
		GameObject o = Selection.activeGameObject;
		GUILayout.EndHorizontal();
		if(o){
			p = o.GetComponent<PathMovement>();
			if(p){
				if(GUILayout.Button("Add")){
					Vector3 pos = o.transform.position;
					p.path.Add(new Vector2(pos.x, pos.y));
				}
				EditorGUILayout.BeginVertical();
				for(int c=0;c<p.path.Count; c++){
					GUILayout.Label("Point "+c);
					GUILayout.BeginHorizontal();
					EditorGUILayout.TextField("x",p.path[c].x.ToString());
					EditorGUILayout.TextField("y",p.path[c].y.ToString());
					if(GUILayout.Button("Move to")){
						Vector3 temp = new Vector3();
						temp.x = p.path[c].x;
						temp.y = p.path[c].y;
						temp.z = o.transform.position.z;
						o.transform.position = temp;
					}
					if(GUILayout.Button("Change")){
						Vector3 pos = o.transform.position;
						p.path.RemoveAt(c);
						p.path.Insert(c,new Vector2(pos.x, pos.y));
					}
					if(GUILayout.Button("Remove")){
						p.path.RemoveAt(c);
					}
					GUILayout.EndHorizontal();
				}
				EditorGUILayout.EndVertical();
			}
		}
	}
}
