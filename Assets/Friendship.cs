using UnityEngine;
using System.Collections;

public class Friendship : Character {
	PathMovement movement;
	public float travelSpeed;

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		movement = GetComponent<PathMovement>();
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
	}
	
	public void goToNextPoint() {
		Vector3 p = movement.getNextObjective();
		new OTTween(transform, 1/travelSpeed*((p-transform.position).magnitude)/10f, OTEasing.SineInOut).Tween("position", p);
	}
	
	void OnCollisionEnter(Collision c) {
		Character e = c.gameObject.GetComponent<Character>();
		if (e is Enemy) {
			e.kill();
		}
	}
	
	public override void damage ()
	{
		
	}
	
	public override void kill ()
	{
		
	}
	
	public override string getName ()
	{
		return "Friendship";
	}
}
