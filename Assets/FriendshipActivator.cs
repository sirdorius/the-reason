using UnityEngine;
using System.Collections;

public class FriendshipActivator : MonoBehaviour {
	Friendship friendship;
	
	void Start() {
		friendship = GameObject.Find ("Friendship").GetComponent<Friendship>();
	}
	void OnTriggerEnter(Collider c) {
		if (c.tag == "basil") {
			friendship.goToNextPoint();
			Destroy(gameObject);
		}
	}
}
