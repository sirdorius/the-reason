using UnityEngine;
using System;
using System.Collections;

public class Effect : IComparable{
	public int priority;
	public AudioSource audio;
	public int CompareTo(object o){
		return - priority + ((Effect)o).priority;
	}
}
