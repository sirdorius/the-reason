using UnityEngine;
using System.Collections;

public class AchievementKiller : Achievement {
	public int enemies;
	private int enemiesKilled;
	public void enemyKilled(){
		enemiesKilled ++;
	}
	public override bool resolved() {
		return enemiesKilled>=enemies;
	}
	public override string resolvedText(){
		return "Hai ucciso almeno "+enemies+" nemici!!!";
	}
}
