using UnityEngine;
using System.Collections;

public class PowerTrap : Power {
	public float range = 500f;

	void Awake () {
		thisPower = Globals.Powers.Mine;
	}

	protected override void powerStart() {
		var bomb = OT.CreateObject("Bomb");
		bomb.GetComponent<Bomb>().range = range;
		bomb.transform.position = transform.position;
		powerEnd();
	}

	protected override void power() {}

}
