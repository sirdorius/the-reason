using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {
	public float speed {get;set;}
	public Vector3 direction {get;set;}
	private bool justSpawning = true;
	
	// Update is called once per frame
	void Start() {
		rigidbody.AddForce(direction * speed * 1000);
		Invoke ("changeSpawningBool", 0.1f);
	}
	
	void OnTriggerEnter(Collider c) {
		Character hitChar = c.collider.GetComponent<Character>();
		if (hitChar is Character) {
			hitChar.damage();
			Destroy(gameObject);
		}
		else if(!justSpawning && (c.tag == "wall" || c.tag == "lightWall")) {
			Destroy(gameObject);
		}
	}
	
	void changeSpawningBool() {
		justSpawning = false;
	}
}
