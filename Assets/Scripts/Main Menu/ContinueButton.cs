using UnityEngine;
using System.Collections;

public class ContinueButton : Button {
	void OnTriggerStay(Collider c) {
		if(c.collider.tag == "basil"){
			switch(cont.text){
				case "Continue":
					Globals.finishedLevel();
					break;
				case "Normal":
					PlayerPrefs.DeleteAll();
					if(Globals.SaveMode){
						PlayerPrefs.SetString("Difficulty","Normal");
						PlayerPrefs.SetInt("CurrentLevel", Application.loadedLevel+1);
						PlayerPrefs.SetInt("CheckpointLevel", Application.loadedLevel+1);
						PlayerPrefs.Save();
					}
					Globals.finishedLevel();
					break;
			}
		}
	}
}
