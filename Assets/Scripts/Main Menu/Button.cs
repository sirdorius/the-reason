using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
	protected TextMesh start;
	protected TextMesh cont;
	protected TextMesh options;
	protected TextMesh exit;
	protected GameObject basil;

	// Use this for initialization
	void Start () {
		GameObject menu = GameObject.Find("Menu");

		start = menu.transform.Find("Start").Find("Text").GetComponent<TextMesh>();
		cont = menu.transform.Find("Continue").Find("Text").GetComponent<TextMesh>();
		options = menu.transform.Find("Options").Find("Text").GetComponent<TextMesh>();
		exit = menu.transform.Find ("Exit").Find("Text").GetComponent<TextMesh>();

		basil = GameObject.Find("Basil");
	}
}
