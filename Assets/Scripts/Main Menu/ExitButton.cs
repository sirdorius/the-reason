using UnityEngine;
using System.Collections;

public class ExitButton : Button {
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider c) {
		if(c.collider.tag == "basil"){
			switch(exit.text){
				case "Hard":
					PlayerPrefs.DeleteAll();
					if(Globals.SaveMode){
						PlayerPrefs.SetString("Difficulty","Hard");
						PlayerPrefs.SetInt("CheckpointLevel", Application.loadedLevel+1);
						PlayerPrefs.SetInt("CurrentLevel", Application.loadedLevel+1);
						PlayerPrefs.Save();
						Globals.finishedLevel();
					}
					break;
				case "Exit":
					Application.Quit();
					break;
			}
		}
	}
}
