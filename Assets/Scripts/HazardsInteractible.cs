using UnityEngine;
using System.Collections;

public interface HazardsInteractible {
	bool activate();
}
