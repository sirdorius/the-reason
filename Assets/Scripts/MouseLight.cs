using UnityEngine;
using System.Collections;

public class MouseLight : Power {
	public float forceModifier;
	public bool moveWithMouse = true;
	public float explosionDuration;
	public float explosionSize;
	private float explosionCountdown;
	private float curRange;
	private float expandedRange;
	private OTAnimatingSprite lightSprite;
	//private GameObject theLight;

	void Awake () {
		thisPower = Globals.Powers.Explosion;
		lightSprite = transform.FindChild("light").transform.FindChild("lightSprite").GetComponent<OTAnimatingSprite>();
	}
	// Update is called once per frame
	protected override void Update () {
		// Light follows the cursor
		if (moveWithMouse) {
			Vector3 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			p.z = transform.position.z;
			transform.position = p;
			//Screen.showCursor = false;
			if(Input.touchCount>0){
				p = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
				p.z = transform.position.z;
				transform.position = p;
			}
		}
		
		base.Update();
	}
	protected override void power() {
		explosionCountdown -= Time.deltaTime;
		if (lightSprite.rotation > 360)
			lightSprite.rotation %= 360;
		new OTTween(transform, explosionDuration, OTEasing.CircOut).Tween("localScale", new Vector3(explosionSize, explosionSize, 1f));
		new OTTween(lightSprite, 1, OTEasing.QuadOut).Tween("rotation", lightSprite.rotation+130);
		
		Invoke("returnToNormalSize", explosionDuration);
		if(explosionCountdown <= 0)	{			
			gameObject.collider.enabled = false;
			powerEnd();
		}
	}
	
	private void returnToNormalSize() {
		new OTTween(transform, 1, OTEasing.QuadInOut).Tween("localScale", new Vector3(1f, 1f, 1f));
	}

	protected override void powerStart() {
		explosionCountdown = explosionDuration;
		powerEnable = true;
		gameObject.collider.enabled = true;
	}
	void OnTriggerEnter(Collider c) {
		Vector3 dist = (c.transform.position - transform.position);
		dist.z = 0;
		Character hitChar = c.collider.GetComponent<Character>();
		Nail hitNail = c.collider.GetComponent<Nail>();
		
		if (hitChar is Enemy) {
			if(((Enemy) hitChar).lightHit())
				return;
			c.attachedRigidbody.velocity = Vector3.zero;
			float multiplier = 1000f;
			if (hitChar is Anger) multiplier /= 2.5f;
			if (hitChar is Recklessness) multiplier *= 1.6f;
			c.attachedRigidbody.AddForce(dist.normalized*(forceModifier*multiplier), ForceMode.Acceleration);
			hitChar.stun();
		}
		else if (hitNail) {
			// realistic bounce back
			//c.attachedRigidbody.velocity = dist.normalized*c.attachedRigidbody.velocity.magnitude;
			// simple bounce back to spitter
			c.attachedRigidbody.velocity *= -1;
			hitNail.createdBy = null;
		}
		//gameObject.collider.enabled = false;
	}

	void switchMouseControlled () {
		moveWithMouse = !moveWithMouse;
	}
}
