using UnityEngine;
using System.Collections;

public class Sadness : Enemy {
	public float tearFrequency;
	public float tearDuration;
	private static Tears tears;
	private float timeSinceLast = 0;
	
	// TODO
	override protected void Start() {
		base.Start ();
		OT.PreFabricate("Tears", 50);
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update ();
		timeSinceLast += Time.deltaTime*Time.timeScale;
		if (timeSinceLast > tearFrequency) {
			timeSinceLast = 0;
			var tears = OT.CreateObject("Tears");
			tears.transform.position = transform.position + new Vector3(30,18,0);
			tears.GetComponent<Tears>().timeToLive = tearDuration;
			tears.GetComponent<Tears>().damageSameType = damageSameType;
		}
	}
		
	public override string getName (){
		 return "Sadness";
	}
}
