using UnityEngine;
using System.Collections;

public class Fear : Enemy {
	public float timeToDarken;
	public float rangeMultiplier;
	public float flashTime;
	private GameObject theLight;

	private OTAnimatingSprite eyes;

	private static AudioClip startAudio;
	private static AudioClip lifeAudio;
	private static AudioClip deathAudio;
	private bool isPlaying;
	
	private OTSprite ambientLight;
	private OTTween alphaTween, scaleTween;

	// Use this for initialization
	override protected void Start () {
		base.Start();
		killWithLight = true;
		if(!startAudio) startAudio = (AudioClip) Resources.Load("Sound/Ambience/breathofdeath");
		if(!lifeAudio) lifeAudio = (AudioClip) Resources.Load("Sound/Ambience/sottofondo di disagio");
		theLight = GameObject.Find("TheLight");

		audio.maxDistance = 2000;
		isPlaying = true;
		Music.playEffect(audio, startAudio,1);
		
		
		Globals.fearCount++;
		ambientLight = GameObject.Find("TheLight").transform.FindChild("light").transform.FindChild("darkness").GetComponent<OTSprite>();
		ambientLight.alpha = 1f;
		theLight.transform.localScale = new Vector3(rangeMultiplier, rangeMultiplier, 1f);
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
		
		if(isPlaying && audio.time>startAudio.length-3.5){
			if(!AudioListener.pause) Music.playEffect(audio, lifeAudio,1);
			isPlaying = false;
		}
	}

	override public void kill() {
		renderer.enabled = false;
		collider.enabled = false;
		foreach (Transform child in transform)
			child.renderer.enabled = false;
		Globals.fearCount--;
		lighten ();
		if (Globals.fearCount > 0)
			Invoke("darken", timeToDarken);
		
		Basil o = player.GetComponent<Basil>();
		o.sighAudio();
		Invoke("safeDestroy", timeToDarken+1);
		Instantiate(Resources.Load("Effects/EnemyDestroyedPS"), transform.position, Quaternion.identity);
	}
	
	private void darken() {
		if (alphaTween.isRunning) return;
		if (scaleTween.isRunning) return;
		alphaTween = new OTTween(ambientLight, timeToDarken, OTEasing.QuadIn).Tween("alpha", 1f);
		scaleTween = new OTTween(theLight.transform, timeToDarken, OTEasing.SineInOut).Tween("localScale", new Vector3(rangeMultiplier, rangeMultiplier, 1f));
	}
	
	private void lighten() {
		if (alphaTween != null) alphaTween.Stop();
		if (scaleTween != null) scaleTween.Stop();
		alphaTween = new OTTween(ambientLight, timeToDarken, OTEasing.CircIn).Tween("alpha", 0.5f);
		scaleTween = new OTTween(theLight.transform, timeToDarken, OTEasing.SineInOut).Tween("localScale", new Vector3(1f, 1f, 1f));
	}
	
	
	protected override void flip(bool v){
		base.flip (v);
		if(!eyes)
			eyes = transform.GetChild(0).GetComponent<OTAnimatingSprite>();
		eyes.flipHorizontal = v;
		if(v){
			eyes.transform.position += Vector3.right*12;
		}
		else {
			eyes.transform.position -= Vector3.right*12;
		}
	}
	
	protected override void playerEntersRange() {
	}
	
	protected override void playerExitsRange() {
	}
		
	public override string getName (){
		 return "Fear";
	}
}
