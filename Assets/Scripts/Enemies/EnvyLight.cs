using UnityEngine;
using System.Collections;

public class EnvyLight : MonoBehaviour {
	private GameObject player;
	public float cooldown;
	public float duration;
	public float forceMagnitude;
	public float preparation;
	public bool hitEnemies;
	private bool explosion;
	private float cooldownCountdown;
	private float durationCountdown;
	private float preparationCountdown;
	private Vector2 curRange;

	private OTSprite lightSprite;
	private GameObject envy;
	
	void Start () {
		player = GameObject.Find ("Basil");
		envy = GameObject.Find("Envy");

		cooldownCountdown = cooldown;
		preparationCountdown = preparation;
		durationCountdown = duration;
		explosion = false;
		lightSprite = transform.GetChild(0).GetComponent<OTSprite>();
		curRange = lightSprite.size;
	}
	
	// Update is called once per frame
	void Update () {
		float dt = Time.deltaTime*Time.timeScale;
		if(cooldownCountdown > 0 && !explosion) cooldownCountdown -= dt;
		if(cooldownCountdown <= 0 && !explosion){
			Vector3 p = new Vector3 (player.transform.position.x, player.transform.position.y, transform.position.z);
			//gameObject.light.enabled = true;
			gameObject.collider.enabled = true;
			new OTTween(transform, 0.1f, OTEasing.Linear).Tween("position", p);
			new OTTween(lightSprite, duration*10f, OTEasing.CircOut).Tween("size", curRange*1.5f);
			explosion = true;
		}
		
		if(preparationCountdown > 0 && durationCountdown > 0 && explosion) preparationCountdown -= dt;
		if(preparationCountdown <= 0 && durationCountdown > 0 && explosion) {
;
			durationCountdown -= dt;
			new OTTween(lightSprite, duration*10f, OTEasing.CircOut).Tween("size", curRange);
			// Non funziona
			//new OTTween(transform, 0.1f, OTEasing.Linear).Tween("position", envy.transform.position);

			if(durationCountdown <= 0)	{
				durationCountdown = duration;
				cooldownCountdown = cooldown;
				preparationCountdown = preparation;
				explosion = false;
				gameObject.collider.enabled = false;
			}
		}
	}
	
	void OnTriggerStay(Collider c) {
		if(durationCountdown > 0 && preparationCountdown <= 0) {
			Character hitChar = c.collider.GetComponent<Character>();
			if (!(hitChar is Basil) && (hitEnemies && !(hitChar is Enemy))) return;
			Vector3 lightPos = transform.position;
			Vector3 otherPos = hitChar.transform.position;
			lightPos.z = 0;
			otherPos.z = 0;
			Vector3 dist = otherPos-lightPos;
			dist = dist.normalized*(((SphereCollider)collider).radius-dist.magnitude)*forceMagnitude*10;
			hitChar.rigidbody.AddForce( dist , ForceMode.Acceleration);
			hitChar.stun();			
		}
	}
}
