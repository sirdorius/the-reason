using UnityEngine;
using System.Collections;

public class Recklessness : Enemy {
	public float chargeSpeed;
	public float chargeTime;
	public float animationTime;
	public bool chargeInStraightLine = false;
		
	private enum State {Normal=0, ChargeAnimation, Charging};
	private State state = State.Normal;
	private Vector3 initialScale;
	private float chargeStartTime = 0;
	private AudioClip clip;

	// Use this for initialization
	override protected void Start () {
		base.Start();
		initialScale = transform.localScale;
		clip = (AudioClip) Resources.Load("Sound/Recklessness/66132__robinhood76__00822-monster-2-voice");
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
		if (state == State.ChargeAnimation) {
			gameObject.transform.localScale -= new Vector3 (50*Time.deltaTime*Time.timeScale,0,0);
		}
	}
	
	override protected void FixedUpdate() {
		base.FixedUpdate();
		base.checkPlayerPosition();
	}
	
	protected override void playerEntersRange(){
		var movement = GetComponent<Movement>();
		if (movement)
			movement.enabled = false;
	}
	protected override void playerStaysInRange(){
		if (state == State.Normal && !stunned){
			if (chargeInStraightLine) {
				var deltaX = player.transform.position.x - transform.position.x;
				var deltaY = player.transform.position.y - transform.position.y;
				if (Mathf.Abs(deltaX) < 40) {
					StartCoroutine (playChargeAnimation(new Vector3(0, deltaY/Mathf.Abs(deltaY), 0)));
				}
				else if (Mathf.Abs(deltaY )< 40) {
					StartCoroutine (playChargeAnimation(new Vector3(deltaX/Mathf.Abs(deltaX), 0, 0)));
				}
			}
			else
				StartCoroutine(playChargeAnimation(Vector3.zero));
		}
	}
	
	protected override void playerExitsRange() {
		base.playerExitsRange();
	}
	
	// Charge in current direction of player if given vector is Zero, else charge in the given direction after a time
	private IEnumerator playChargeAnimation(Vector3 dir) {
		Music.playEffect(audio,clip,3);
		state = State.ChargeAnimation;
		yield return new WaitForSeconds(animationTime);
		if (dir == Vector3.zero) {
			dir = player.transform.position-transform.position;
			dir.z = 0;
			charge(dir);
		}
		else
			charge(dir);
	}
	
	private void charge(Vector3 direction) {
		state = State.Charging;
		gameObject.transform.localScale = initialScale;
		direction = direction.normalized;
		chargeStartTime = Time.time;
		StartCoroutine(secondCharge(direction));
		StartCoroutine(stopCharge());
	}
	
	private IEnumerator secondCharge(Vector3 direction) {
		yield return new WaitForSeconds(0.02f);
		if (Time.time - chargeStartTime > chargeTime/4f) {
			yield break;
		}
		rigidbody.AddForce(chargeSpeed*1000*direction, ForceMode.Acceleration);
		StartCoroutine(secondCharge (direction));
	}
	
	IEnumerator stopCharge() {
		yield return new WaitForSeconds(chargeTime);
		state = State.Normal;
	}
			
	public override void stun() {
		StopCoroutine("secondCharge");
		StopCoroutine("playChargeAnimation");
		rigidbody.drag = 15;
		base.stun ();
		state = State.Normal;
		Invoke ("resetDrag", stunTimer);
	}
	
	private void resetDrag() {
		rigidbody.drag = 5;
	}
	
	public override string getName (){
		 return "Recklessness";
	}
}
