using UnityEngine;
using System.Collections;

public class Envy : Enemy {

	void Start () {
		base.Start();
		Globals.closeExit();
	}
	
	override public void kill() {
		GameObject envyLight = GameObject.Find ("EnvyLight");
		Globals.restoreExit();
		renderer.enabled = false;
		collider.enabled = false;
		Destroy(envyLight);
		Destroy(gameObject);
		Globals.changePower(Globals.Powers.Explosion);
		base.kill ();
	}
	
	public override string getName (){
		 return "Envy";
	}
}
