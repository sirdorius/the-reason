using UnityEngine;
using System.Collections;

public class Paranoia : Enemy {
	public float teleportRecharge;
	public float teleportTime = 0.2f;
	
	private PathMovement movement;
	private OTTween posTween;
	private OTTween alphaTween;

	// Use this for initialization
	override protected void Start () {
		base.Start();
		killWithLight = true;
		Globals.closeExit();
		movement = GetComponent<PathMovement>();
		if (movement != null && movement.enabled) Invoke("teleport", 0f);
	}
	
	public void startMovement() {
		if (movement) {
			Invoke("teleport", 0f);
		}
	}
	
	override protected void Update() {
		base.Update();
	}
	
	void teleport() {
		var newpos = movement.getNextObjective();
		collider.enabled = false;
		posTween = new OTTween(transform, teleportTime, OTEasing.ExpoOut).Tween("position", newpos);
		GetComponent<OTSprite>().alpha = 0f;
		alphaTween = new OTTween(GetComponent<OTSprite>(), teleportTime, OTEasing.ExpoOut).Tween("alpha", 1f);
		//alphaTween.onTweenFinish=reappear;
		Invoke ("reappear", teleportTime);
		Invoke("teleport", teleportRecharge);
	}
	
	void reappear() {
		collider.enabled = true;
		GetComponent<OTSprite>().alpha = 1;
	}
	
	public override void kill ()
	{
		Globals.restoreExit();
		if(posTween != null) posTween.Stop();
		if(alphaTween != null) alphaTween.Stop();
		StopCoroutine("reappear");
		base.kill ();
	}
	
	public override string getName (){
		 return "Paranoia";
	}
}
