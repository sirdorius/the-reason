using UnityEngine;
using System.Collections;

public class Anger : Enemy {
	public int lives;

	// Use this for initialization
	override protected void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
	}
	
	override protected void FixedUpdate() {
		base.FixedUpdate();
		base.checkPlayerPosition();
	}
	
	protected override void playerStaysInRange(){
		goToPoint(player.transform.position, 1);
	}
	
	override public void damage() {
		lives--;
		if (lives <= 0) 
			kill ();
	}
	
	void OnCollisionEnter(Collision c) {
		if(c.gameObject.tag == "wall" && c.gameObject.name != "borderleft" && c.gameObject.name != "borderbottom" && 
			c.gameObject.name != "bordertop" && c.gameObject.name != "borderright")
			Destroy (c.gameObject);
	}
		
	public override string getName (){
		 return "Anger";
	}
}
