using UnityEngine;
using System.Collections;

public class SelfHarm : Enemy {
	public float suicideTime;
	public float explosionDuration;
	// public float explosionRange TODO
	
	private enum State {Normal=0, SuicideAnimation};
	private State state = State.Normal;
	private bool invulnWhileExploding = true;
	
	override protected void Start() {
		base.Start ();
	}

	// Update is called once per frame
	override protected void Update () {
		base.Update ();
		if (state == State.SuicideAnimation) {
			gameObject.transform.localScale += new Vector3 (21.9f*Time.deltaTime*Time.timeScale,30*Time.deltaTime*Time.timeScale,0);
		}
	}
	
	void FixedUpdate() {
		base.checkPlayerPosition();
	}
	
	protected override void playerStaysInRange(){
		if (state != State.SuicideAnimation)
			goToPoint(player.transform.position, 1);
	}
	
	void OnCollisionEnter(Collision c) {
		if (c.collider.tag == "basil"){
			StartCoroutine(suicide());
		}
	}
	
	IEnumerator suicide() {
		state = State.SuicideAnimation;
		yield return new WaitForSeconds(suicideTime);
		var explosion = OT.CreateObject("Explosion");
		var explosionScript = explosion.GetComponent<Explosion>();
		explosionScript.timeToLive = explosionDuration;
		explosionScript.damageSameType = damageSameType;
		explosion.transform.position = transform.position;
		safeDestroy();
	}
	
	public override void damage() {
		if (invulnWhileExploding && state == State.SuicideAnimation)
			return;
		else
			base.kill ();
	}
	
	public override string getName (){
		 return "Selfharm";
	}
	
	override protected void OnCollisionStay(Collision c) {}
}
