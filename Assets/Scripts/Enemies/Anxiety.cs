using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Anxiety : Enemy {
	public float reloadTime;
	public float shootTime;
	public int magazineSize;
	public float nailSpeed;

	private static AudioClip spit;
	private static Nail nail;
	private float time;
	private int bullet;

	// Use this for initialization
	override protected void Start () {
		base.Start();
		time = shootTime+reloadTime;
		bullet = 1;
		spit = (AudioClip) Resources.Load("Sound/Anxiety/118193__bmcken__spitting");
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
		time += Time.deltaTime*Time.timeScale;
	}
	
	override protected void FixedUpdate() {
		base.FixedUpdate();
		base.checkPlayerPosition();
	}
	
	protected override void playerEntersRange() {
	}
	
	protected override void playerStaysInRange(){
		shoot();
	}
	
	private void shootSound(){
		Music.playEffect(audio, spit, 3);
	}
	
	protected override void playerExitsRange() {
	}
	
	void shoot() {
		if(Time.timeScale*time>(shootTime+reloadTime)*Time.timeScale){
			var nail = OT.CreateObject("Nail");
			nail.transform.position = transform.position+new Vector3(29,0,0);
			var script = nail.GetComponent<Nail>();
			script.createdBy = gameObject;
			script.speed = nailSpeed;
			script.damageSameType = damageSameType;
			script.setDirection(basilDirection);
			shootSound();

			if(bullet<magazineSize)
				time = reloadTime;
			else{
				time = 0;
				bullet = 0;
			}
			bullet ++;
		}
	}
	
	public override string getName (){
		 return "Anxiety";
	}
}
