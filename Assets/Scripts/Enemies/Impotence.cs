using UnityEngine;
using System.Collections;

public class Impotence : Enemy {
	private bool touchingPlayer = false;
	private AudioClip clip;

	override protected void Start () {
		base.Start();
		clip = (AudioClip) Resources.Load("Sound/Impotence/157027__slave2thelight__solo-zombie-1");
	}

	// Update is called once per frame
	override protected void Update () {
		base.Update();
	}
	
	override protected void FixedUpdate() {
		base.FixedUpdate();
		base.checkPlayerPosition();
	}
	
	void OnCollisionEnter(Collision c) {
		if (c.collider.tag == "basil")
			touchingPlayer = true;
	}
	
	void OnCollisionExit(Collision c) {
		if (c.collider.tag == "basil")
			touchingPlayer = false;
	}
	
	protected override void playerStaysInRange(){
		if (!touchingPlayer) {
			goToPoint(player.transform.position, decelerateWhenDistanceIsLessThan);
			Music.playEffect(audio, clip, 0);
		}
	}
	
	public override string getName (){
		 return "Impotence";
	}
	
	override protected void OnCollisionStay(Collision c) {}
}
