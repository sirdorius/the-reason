using UnityEngine;
using System.Collections;
using System.Collections.Generic;

abstract public class Enemy : Character {
	public float range;			// range in which enemy can see player
	public bool canSeeThroughWalls = true;
	public bool followsPlayerThroughHazards = true; // not working perfectly, but it's better than nothing
	public bool damageSameType = true; // damage monsters of the same type?
		
	protected bool playerInRange;
	protected GameObject player;
	protected Vector3 basilDirection;
	protected float basilDistance;
	protected bool killWithLight = false;
	
	protected bool isQuitting = false;
	private Movement movementScript;

	// Use this for initialization
	void Awake () {
		playerInRange = false;
		player = GameObject.Find("Basil");
		movementScript = GetComponent<Movement>();
	}
	
	/*void Start() {
		base.Start ();
	}
	
	void Update() {
		base.Update ();
	}*/
	
	void OnApplicationQuit() {
		isQuitting = true;
	}
	
	protected void createParticles() {
		// if the Unity scene is not being closed create particles
		if (isQuitting) return;
		Instantiate(Resources.Load("Effects/EnemyDestroyedPS"), transform.position, Quaternion.identity);
	}
	
	// Update is called once per frame
	protected void checkPlayerPosition () {
		basilDirection = player.transform.position - transform.position;
		basilDirection.z = 0;
		basilDistance = basilDirection.magnitude;
		basilDirection = basilDirection.normalized;

		// fire events when player enters or exits the range
		if (basilDistance < range) {
			// raycast on wall layer
			if (!canSeeThroughWalls) {
				if(Physics.Raycast(transform.position, basilDirection, basilDistance, 1<<9))
					return;
			}
			// raycast on hazards layer
			if (!followsPlayerThroughHazards) {
				if(Physics.Raycast(transform.position, basilDirection, basilDistance, 1<<10))
					return;
			}
			
			// player is in range
			if (playerInRange){
				playerStaysInRange();
			}
			else {
				playerInRange = true;
				playerEntersRange();
			}
		}
		else {
			if (playerInRange) {
				playerInRange = false;
				playerExitsRange();
			}
		}
	}

	// Events for handling when player comes in range. Should be overridden by each enemy AI
	protected virtual void playerEntersRange(){
		if (movementScript)
			movementScript.enabled = false;
	}
	protected virtual void playerStaysInRange(){}
	protected virtual void playerExitsRange(){
		if (movementScript)
			movementScript.enabled = true;
	}
	
	public override void kill() {
		if(audio) Music.removeEffect (audio);
		createParticles();
		GameObject g = GameObject.Find("Achievements");
		if(g){
			AchievementKiller a = g.GetComponent<AchievementKiller>();
			a.enemyKilled();
		}
		safeDestroy();
	}
	
	protected void safeDestroy() {
		var r = gameObject.GetComponent<Respawner>();
		if (r == null)
			Destroy(gameObject);
		else
			r.respawn();
	}
	
	public override void damage() {
		kill();
	}
	
	protected virtual void OnCollisionStay(Collision c) {
		Character hitChar = c.collider.GetComponent<Character>();
		if(hitChar) {
			if (!damageSameType && GetType().Equals(hitChar.GetType())) 
				return;
			if (hitChar is Character) {
				hitChar.damage();
			}
		}
	}
	
	public bool lightHit() {
		if(killWithLight) {
			kill();
			return true;
		}
		else
			return false;
	}
	
}
