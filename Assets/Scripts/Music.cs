using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Music : MonoBehaviour {
	public enum AvailableMusic {NoChange, Default, Sad, Happy,
		Defiance, Release, MysticDream, ClownHysteria, TheRainingCave, MarchOfTheDamned, Love};
	
	public AvailableMusic song;
	private static string currentSong;
	
	private static Music instance = null;
	private static List<Effect> callers;
	private static int numEffetcs = 10;
	private static int numActiceEffetcs = 5;
	public static float volumeEffect = 1;

	public static Music Instance {
		get { return instance; }
	}
	
	static string GetSongName(AvailableMusic s) {
		switch (s) {
		case AvailableMusic.Defiance: 
			return "500422_Defiance";
		case AvailableMusic.ClownHysteria: 
			return "487303_Clown-Hysteria";
		case AvailableMusic.Release: 
			return "500970_Release";
		case AvailableMusic.TheRainingCave: 
			return "511161_The-Raining-Cave";
		case AvailableMusic.MarchOfTheDamned: 
			return "508331_March-of-the-Damned";
		case AvailableMusic.MysticDream: 
			return "502253_Mystic-Dream";
		case AvailableMusic.Love: 
			return "299347_Love";
		case AvailableMusic.NoChange: 
			return currentSong;
		default:
			return "error";
		}
	}
	
	void Awake () {
		currentSong = GetSongName(song);
		
		callers = new List<Effect>(numEffetcs);
		if(instance != null && instance != this){
			Destroy(this.gameObject);
			changeAudio();
            return;
		}
		else {
			instance = this;
			changeAudio();
			DontDestroyOnLoad(this.gameObject);
		}
	}
	public void changeAudio(){
		string name = GetSongName(song);
		if(instance.audio.clip == null || instance.audio.clip.name != name) {
			instance.audio.clip = (AudioClip) Resources.Load("Sound/music/"+name);
			instance.audio.Play();
		}
	}

	public static void setMusic(float v){
		instance.audio.volume = v;
	}
	
	public static void setEffect(float v){
		volumeEffect = v;
	}
	public static bool musicMute(){
		return instance.audio.volume==0;
	}
	public static bool effectMute(){
		return volumeEffect==0;
	}

	public static void playEffect(AudioSource a, AudioClip clip, int priority){
		removeInactiveEffect();
		Effect other = containsEffect(a);
		if(other !=null ){
			if(other.priority<= priority){
				return;
			}
			else {
				a.clip = clip;
				other.audio = a;
				return;
			}
		}
		
		if(callers.Count<numEffetcs){
			Effect e = new Effect();
			a.clip = clip;
			e.audio = a;
			e.priority = priority;
			callers.Add(e);
			callers.Sort();
			if(Globals.gameState != Globals.GameState.Paused && Globals.gameState != Globals.GameState.LevelStart){
				a.Play();
				for(int i=0; i<callers.Count; i++){
					Effect e1 = callers[i];
					if(e1.audio && e1.audio.isPlaying){
						if(i<numActiceEffetcs){
							e1.audio.volume = volumeEffect;
						}
						else {
							e1.audio.volume = volumeEffect/2;
						}
					} else {
						callers.RemoveAt(i);
					}
				}
			}	
		}
	}

	public static void effectsPause(){
		for(int i=0;i<callers.Count;i++){
			Effect e = callers[i];
			if(e.audio) e.audio.Pause();
			else {
				callers.RemoveAt(i);
				i--;
			}
		}
	}

	public static void effectsPlay(){
		foreach(Effect e in callers){
			if(e.audio) e.audio.Play();
		}
	}
	
	public static void removeInactiveEffect(){
		if(Globals.gameState != Globals.GameState.Paused && Globals.gameState != Globals.GameState.LevelStart){
			Effect e;
			for(int i = 0; i<callers.Count; i++){
				e = callers[i];
				if(!e.audio || !e.audio.isPlaying){
					callers.RemoveAt(i);
					i--;
				}
			}
		}
	}
	public static Effect containsEffect(AudioSource a){
		Effect effect = null;
		for(int i=0; i<callers.Count;i++){
			Effect e = callers[i];
			if(e.audio == a){
				effect = e;
			}else {
				callers.RemoveAt(i);
				i--;
			}
		}
		return effect;
	}
	public static void removeEffect(AudioSource a){
		if(Globals.gameState != Globals.GameState.Paused && Globals.gameState != Globals.GameState.LevelStart){
			for(int i=0; i<callers.Count; i++){
				Effect e = callers[i];
				if(e.audio == a){
					callers.RemoveAt(i);
					i--;
				}
			}
		}
	}
}
