using UnityEngine;
using System.Collections;

public abstract class Achievement : MonoBehaviour {
	public abstract bool resolved () ;
	public abstract string resolvedText() ;
}
