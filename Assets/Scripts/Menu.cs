using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	private GameObject player;
	private Texture mouseTexture;
	
	void Start() {
		player = GameObject.Find ("Basil");
		mouseTexture = Resources.Load("Textures/mouse") as Texture;
	}

	void OnGUI() {
		int left = Screen.width/2 - 150;
		int top = Screen.height/2 - 175;
		if(Globals.gameState == Globals.GameState.Dead) {
			if(GUI.Button(new Rect(left, (Screen.height/2 - 25), 300, 50), "Restart level (or press R)") ||
				(Event.current.keyCode == KeyCode.R && Event.current.type == EventType.KeyUp)) {
				Globals.restartLevel();
			}
		}
		else if(Globals.gameState == Globals.GameState.Playing) {
			if (Event.current.keyCode == KeyCode.Escape && Event.current.type == EventType.KeyUp) {
				if(!Application.loadedLevelName.StartsWith("intro")){
					Globals.gameState = Globals.GameState.Paused;
					Time.timeScale = 0;
				}
				else {
					Application.LoadLevel("Level1");
				}
			}
			if (Event.current.keyCode == KeyCode.R && Event.current.type == EventType.KeyUp) {
				Globals.restartLevel();
			}
			if (Event.current.keyCode == KeyCode.M && Event.current.type == EventType.KeyUp) {
				AudioListener.pause = !AudioListener.pause;
			}
			if (Event.current.keyCode == KeyCode.N && Event.current.type == EventType.KeyUp) {
				Globals.finishedLevel();
			}
			if (Event.current.keyCode == KeyCode.B && Event.current.type == EventType.KeyUp) {
				Globals.previousLevel();
			}
		}
		else if(Globals.gameState == Globals.GameState.Paused) {
			if(GUI.Button(new Rect(left, top, 300, 50), "Restart level (or press R)") ||
				(Event.current.keyCode == KeyCode.R && Event.current.type == EventType.KeyUp)) {
				Globals.restartLevel();
			}
			if(GUI.Button(new Rect(left,top + 50, 300 ,50), "Unpause game (or press ESC)") ||
				(Event.current.keyCode == KeyCode.Escape && Event.current.type == EventType.KeyUp)) {
				Globals.changeState(Globals.GameState.LevelStart);
			}
			if(GUI.Button(new Rect(left, top + 100, 300, 50), "Music on/off") || 
				(Event.current.keyCode == KeyCode.M && Event.current.type == EventType.KeyUp)) {
				if(Music.musicMute()){
					Music.setMusic(1);
				}
				else {
					Music.setMusic(0);
				}
			}
			if(GUI.Button(new Rect(left, top + 150, 300, 50), "Effects on/off") ||
				(Event.current.keyCode == KeyCode.M && Event.current.type == EventType.KeyUp)) {
				if(Music.effectMute())
					Music.setEffect(1);
				else Music.setEffect(0);
			}
			if(GUI.Button(new Rect(left, top + 200, 300, 50), "Next Level") ||
				(Event.current.keyCode == KeyCode.N && Event.current.type == EventType.KeyUp)) {
				Globals.finishedLevel();
			}
			if(GUI.Button(new Rect(left, top + 250, 300, 50), "Previous Level") ||
				(Event.current.keyCode == KeyCode.B && Event.current.type == EventType.KeyUp)) {
				Globals.previousLevel();
			}
			if(GUI.Button(new Rect(left, top + 300, 300, 50), "Exit")) {
				Application.Quit();
			}
			
		}
		else if(Globals.gameState == Globals.GameState.LevelStart) {
			Time.timeScale = 0f;
			Vector3 p = Camera.main.WorldToScreenPoint(player.transform.position);
			GUI.DrawTexture(new Rect(p.x-20,Camera.main.pixelHeight-p.y-155,150,118), mouseTexture);
		}
	}
}
