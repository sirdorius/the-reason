using UnityEngine;
using System.Collections;

public class Tears : MonoBehaviour {
	
	public float timeToLive {get;set;}
	private Vector3 initialScale;
	private float timeCreated = 0f;
	private OTSprite sprite;
	public bool damageSameType {get;set;}

	// Use this for initialization
	void Start () {
		OT.Destroy(gameObject, timeToLive);
		//Destroy(gameObject, timeToLive);
		sprite = GetComponent<OTSprite>();
		initialScale = sprite.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		timeCreated += Time.deltaTime*Time.timeScale;
		sprite.alpha = Mathf.Lerp(1f, 0.6f, timeCreated/timeToLive);
		sprite.transform.localScale = initialScale * Mathf.Lerp(1f, 0.6f, timeCreated/timeToLive);
	}
	
	void OnTriggerEnter(Collider c) {
		Character hitChar = c.collider.GetComponent<Character>();
		if (damageSameType && hitChar is Enemy && !(hitChar is Sadness))
			hitChar.damage();
		else if (hitChar is Basil) {
			hitChar.damage();
		}
	}
}
