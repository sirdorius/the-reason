using UnityEngine;
using System.Collections;

public class Respawner : MonoBehaviour {
	public float respawnTime = 3f;
	private Vector3 initPosition;
	private Vector3 initScale;
	private Character type;
	private Level levelScript;

	// Use this for initialization
	void Start () {
		initPosition = transform.position;
		initScale = transform.localScale;
		type = gameObject.GetComponent<Character>();
		levelScript = GameObject.Find ("Template").GetComponent<Level>();
	}
	
	public void respawn() {
		levelScript.spawnRequest(gameObject, initPosition, respawnTime);
		Destroy(gameObject);
	}
	
	public void timedSpawn() {
		print (type.name);
		transform.position = initPosition;
		transform.localScale = initScale;
		gameObject.SetActive(true);
		gameObject.GetComponent<OTSprite>().alpha = 1f;
		foreach(MonoBehaviour s in gameObject.GetComponents<MonoBehaviour>()) {
			s.enabled = true;
		}
		if(rigidbody) rigidbody.velocity = Vector3.zero;
		
	}
}
