using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
	public float timeToLive {get;set;}
	public bool damageSameType {get;set;}
	private static AudioClip clip;

	// Use this for initialization
	void Start () {
		if(!clip) clip = (AudioClip) Resources.Load("Sound/SelfHarm/boom");
		Music.playEffect(audio, clip,1);
		Destroy(gameObject, timeToLive);
	}
	
	void OnCollisionEnter(Collision c) {
		Character hitChar = c.collider.GetComponent<Character>();
		if (!damageSameType && hitChar is SelfHarm)
			return;
		if (hitChar is Character) {
			hitChar.damage();
		}
	}
}
