using UnityEngine;
using System.Collections;

public class Love : Character {
	private GameObject jealousy;
	private Vector3 shieldScale;
	const float SHIELDANIMTIME = 0.3f;
	GameObject shield;
	bool jealousyIsDead = false;

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		jealousy = GameObject.Find ("Jealousy");
		shield = transform.GetChild(0).gameObject;
		shieldScale = shield.transform.localScale;
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
	}
	
	void FixedUpdate() {
		if (jealousy)
			goToPoint(jealousy.transform.position, decelerateWhenDistanceIsLessThan);
		else if (!jealousyIsDead) {
			jealousyIsDead = true;
			kill ();
		}
	}
	
	public override void damage (){
		
	}
	
	public void jealousyDied() {
		jealousy = null;
	}
	
	public override void kill ()
	{
		// deactivate all colliders
		collider.enabled = false;
		StartCoroutine(killAnimation());
	}
	
	private IEnumerator killAnimation() {
		// TODO non funziona
		var m = GameObject.Find("Music").GetComponent<Music>();
		m.song = Music.AvailableMusic.Love;
		m.changeAudio();
		shieldActive(false, 0f);
		new OTTween(transform, 1f, OTEasing.QuadIn).Tween("localScale", transform.localScale*1.5f);
		yield return new WaitForSeconds(1f);
		new OTTween(transform, 2f, OTEasing.QuadIn).Tween("localScale", Vector3.zero);
		new OTTween(GetComponent<OTAnimatingSprite>(), 2f, OTEasing.SineIn).Tween("alpha", 0f);
		yield return new WaitForSeconds(2f);
		var p = GameObject.Find("ParanoiaBlocker");
		p.transform.position = transform.position;
		p.GetComponent<PathMovement>().enabled = true;
		p.GetComponent<Paranoia>().startMovement();
		GameObject.Find("Enemies").SetActive(false);
		Destroy (gameObject);
	}
	
	public override string getName (){
		 return "Love";
	}
	
	void OnCollisionEnter(Collision c) {
		if (c.gameObject.name == "Jealousy") {
			stun ();
			StartCoroutine(shieldActive(false, 0));
			StartCoroutine(shieldActive(true, stunTimer));
		}
	}
	
	IEnumerator shieldActive(bool activate, float afterSeconds) {
		yield return new WaitForSeconds(afterSeconds);
		
		if (activate)
			new OTTween(shield.transform, SHIELDANIMTIME, OTEasing.QuadOut).Tween("localScale", shieldScale);
		else
			new OTTween(shield.transform, SHIELDANIMTIME, OTEasing.QuadOut).Tween("localScale", Vector3.zero);
		//transform.GetChild(0).renderer.enabled = transform.GetChild(0).collider.enabled = activate;
	}
}
