using UnityEngine;
using System.Collections;

public class Dear : MonoBehaviour {
	private bool caught = false;
	void OnTriggerEnter(Collider c) {
		if(c.tag=="basil"){
			PlayerPrefs.SetInt(gameObject.name,1);
			PlayerPrefs.Save();
			caught = true;
			Destroy(gameObject);
		}
	}
	void OnDestroy() {
		if(!caught)
			PlayerPrefs.SetInt(gameObject.name,-1);
	}
}
