using UnityEngine;
using System.Collections;

public class PowerSelection : MonoBehaviour {
	private Power powerExplosion;
	private Power powerShield;
	private Power powerMine;
	private LineRenderer line;
	private Vector3[] points;
	private int prevNum;

	void Start() {
		showPower(Globals.selectedPower);
		line = GetComponent<LineRenderer>();
		powerExplosion = GameObject.Find("TheLight").GetComponent<MouseLight>();
		powerShield = GameObject.Find("TheLight").GetComponent<PowerShield>();
		powerMine = GameObject.Find("Basil").GetComponent<PowerTrap>();
		points = new Vector3[100];
		for(int i = 0; i<100; i++){
			points[i].x = transform.position.x + 50*Mathf.Cos(2*Mathf.PI*(i/(float)99));
			points[i].y = transform.position.y + 50*Mathf.Sin(2*Mathf.PI*(i/(float)99));
			points[i].z = transform.position.z;
		}
		prevNum = 0;
	}
	
	void Update () {
		if (Input.GetAxis("Mouse ScrollWheel") > 0) {
        	Globals.nextPower();
			showPower(Globals.selectedPower);
		}
    	if (Input.GetAxis("Mouse ScrollWheel") < 0) {
			Globals.previousPower();
			showPower(Globals.selectedPower);
		}
		Power power = null;
		if(Globals.selectedPower == Globals.Powers.Explosion){
			power = powerExplosion;
		}
		else if(Globals.selectedPower == Globals.Powers.Wall){
			power = powerShield;
		}
		else if(Globals.selectedPower == Globals.Powers.Mine){
			power = powerMine;
		}
		if(power){
			int num = (int)(100f*(power.cooldownCountdown/power.cooldown));
			if(num>0 && num!=prevNum){
				line.SetVertexCount(num);
				for(int i = 0; i<num; i++){
					line.SetPosition(i, points[i]);
				}
			}
			prevNum = num;
		}
	}
	
	public void showPower(Globals.Powers power) {
		OTAnimatingSprite sprite = gameObject.GetComponent<OTAnimatingSprite>();
		sprite.alpha = 1;
		switch(power) {
		case Globals.Powers.None:
			sprite.alpha = 0;
			break;
		case Globals.Powers.Explosion:
			sprite.frameIndex = 0;
			break;
		case Globals.Powers.Wall:
			sprite.frameIndex = 4;
			break;
		case Globals.Powers.Mine:
			sprite.frameIndex = 1;
			break;
		}
	}
}