using UnityEngine;
using System.Collections;

public class Hallucination : MonoBehaviour {
	public float dissappearTime = 0.3f;
	public float blinkFrequency = 5f;
	
	private int initialLayer;
	private Vector3 initialScale;
	private Vector3 currVelocity;
	private float timeSinceBlink = 0;
	
	void Awake() {
		initialLayer = gameObject.layer;
		initialScale = transform.localScale;
	}
	
	void Update() {
		timeSinceBlink += Time.deltaTime;
		if (timeSinceBlink > blinkFrequency) {
			StartCoroutine(blink());
			timeSinceBlink = 0;
		}
	}
	
	private IEnumerator blink() {
		float blinkDuration = 0.2f;
		OTSprite sprite = GetComponent<OTSprite>();
		new OTTween(sprite, blinkDuration, OTEasing.CircIn).Tween("alpha", 0.2f);
		yield return new WaitForSeconds(blinkDuration);
		new OTTween(sprite, blinkDuration, OTEasing.CircIn).Tween("alpha", 1f);
	}

	void OnTriggerEnter(Collider c) {
		if (c.collider.tag == "light") {
			//GetComponent<MeshRenderer>().enabled = false;
			new OTTween(transform, dissappearTime, OTEasing.CircIn).Tween("localScale", new Vector3(0,0,0));
			foreach(var s in GetComponents<MonoBehaviour>()) {
				s.enabled = false;
			}
			if (rigidbody && !rigidbody.isKinematic) {
				currVelocity = rigidbody.velocity;
				rigidbody.velocity = Vector3.zero;
			}
			GetComponent<Collider>().gameObject.layer = LayerMask.NameToLayer("hallucination");
			
		}
	}
	
	void OnTriggerExit(Collider c) {
		if (c.collider.tag == "light") {
			//GetComponent<MeshRenderer>().enabled = true;
			new OTTween(transform, dissappearTime, OTEasing.CircOut).Tween("localScale", initialScale);
			foreach(var s in GetComponents<MonoBehaviour>()) {
				s.enabled = true;
			}
			if (rigidbody && !rigidbody.isKinematic)
				rigidbody.velocity = currVelocity;
			GetComponent<Collider>().gameObject.layer = initialLayer;
		}
	}
}
