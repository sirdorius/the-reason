using UnityEngine;
using System.Collections;

public class LoveShield : MonoBehaviour {
	public float repulseMultiplier;
	
	void OnCollisionEnter(Collision c) {
		var e = c.gameObject.GetComponent<Character>();
		if (e is Enemy) {
			e.stun();
			var a = c.gameObject.transform.position;
			a.z = 0;
			var b = transform.position;
			b.z = 0;
			e.rigidbody.AddForce((a - b)*repulseMultiplier, ForceMode.VelocityChange);
			return;
		}
	}
}