using UnityEngine;
using System.Collections;

public class Power : MonoBehaviour {
	public float cooldown;
	public float cooldownCountdown;
	protected bool powerEnable = false;
	protected Globals.Powers thisPower;
	// Use this for initialization
	void Start () {
		cooldownCountdown = 0;
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		if(cooldownCountdown > 0) {
				cooldownCountdown -= Time.deltaTime;
		}
		if(Globals.selectedPower == thisPower && Globals.gameState == Globals.GameState.Playing) {
			if(Input.GetMouseButtonDown(0) && !powerEnable && (cooldownCountdown <= 0)) {
				powerStart();
			}
			if(powerEnable){
				power ();
			}
		}
	}
	protected virtual void powerStart() {}
	protected virtual void power() {}
	protected virtual void powerEnd() {
		powerEnable = false;
		cooldownCountdown = cooldown;
	}
}
