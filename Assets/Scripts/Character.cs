using UnityEngine;
using System.Collections;

public abstract class Character : MonoBehaviour, HazardsInteractible {
	public float acceleration;		//0-99
	public float decelerateWhenDistanceIsLessThan = 250;	// when distance to objective is smaller than this value 
															// acceleration slows down to 0 linearly
	//public float maxSpeed;
	public float stunTimer;
	private float specialStunTimer = 0;
	protected bool stunned = false;
	private bool wait = false;
	private OTAnimatingSprite animatingSprite;
	protected float FLIPTHRESHOLD = 85f;
	private GameObject _attractingHole = null;
	public float attractionDistance {get;set;}
	private Vector3 lastPos;
	public GameObject attractingHole {
		get {
			return _attractingHole;
		}
		set {
			_attractingHole = value;
			attractionDistance = 0; //TODO
		}
	}

	private Vector3 direction;
	
	protected virtual void Start() {
		animatingSprite = GetComponent<OTAnimatingSprite>();
	}

	public Vector3 getDirection(){
		return rigidbody.velocity;
	}
	
	// reduce acceleration when near point to avoid 'bounce' effect
	public void goToPoint(Vector3 p, float maxAccelerationDistance) {
		if(!stunned && !wait) {
			direction = p - transform.position;
			direction = Vector3.ClampMagnitude(direction, Mathf.InverseLerp(0, maxAccelerationDistance, direction.magnitude));
			rigidbody.AddForce(direction*acceleration*1000*Time.timeScale, ForceMode.Acceleration);
		}
	}
	
	private IEnumerator WaitAndFree(float t) {
        yield return new WaitForSeconds(t);
		stunned = false;
		wait = false;
    }
	
	protected virtual void Update() {
		if(stunned && !wait) {
			if(specialStunTimer != 0) {
				StartCoroutine(WaitAndFree(specialStunTimer));
				specialStunTimer = 0;
			}
			else
				StartCoroutine(WaitAndFree(stunTimer));
			wait = true;
		}

		if (rigidbody.velocity.x > FLIPTHRESHOLD && !animatingSprite.flipHorizontal)
			flip (true);
		else if (rigidbody.velocity.x < -FLIPTHRESHOLD && animatingSprite.flipHorizontal)
			flip (false);
	}
	
	protected virtual void FixedUpdate() {
		attractionDistance += (lastPos-transform.position).magnitude;
		lastPos = transform.position;
	}
	
	protected virtual void flip(bool v){
		animatingSprite.flipHorizontal = v;
	}
	
	public abstract void damage();
	public abstract void kill();
	public abstract string getName();
	
	public void stun(float timer) {
		specialStunTimer = timer;
		stunned = true;
	}
	
	public virtual void stun() {
		stunned = true;
	}
	
	public bool activate() {
		return true;
	}
}
