using UnityEngine;
using System.Collections;

public class healthPickup : MonoBehaviour {
	public int lives = 1;
	void OnTriggerEnter(Collider c) {
		if(c.collider.tag == "basil"){
			Globals.healthPickup(lives);
			Destroy(gameObject);
		}
	}
}
