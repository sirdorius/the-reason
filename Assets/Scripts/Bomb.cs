using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bomb : MonoBehaviour {
	public float timeToLive = 5f;
	
	public float range {get;set;}
	
	void Awake() {
		Destroy(gameObject, timeToLive);
	}

	void OnTriggerEnter(Collider other) {
		if(other.tag == "enemy") {
			var explosion = OT.CreateObject("MineExplosion");
			explosion.transform.position = transform.position;
			explosion.GetComponent<MineExplosion>().range = range;
			Destroy(gameObject);
		}
	}
}
