using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnablerScript : MonoBehaviour {
	public List<GameObject> items;

	void OnTriggerEnter(Collider other){
		Character c = other.GetComponent<Character>();
		if (c is Basil){
			enableAll();
		}
	}
	
	void enableAll() {
		foreach(var i in items) {
			i.SetActive(true);
		}
	}
}
