using UnityEngine;
using System.Collections;

public class LightWallCollider : MonoBehaviour, HazardsInteractible {
	public bool activate() {
		return transform.parent.GetComponent<LightWall>().canActivateTiles;
	}
}
