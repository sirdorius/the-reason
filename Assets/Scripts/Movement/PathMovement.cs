using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathMovement : Movement {
	private int pathIndex = 0;
	public int threshold = 50;
	public List<Vector2> path;
	public bool loop = true;
	
	private bool _loopedOnce = false;
	public bool loopedOnce {
		get{
			return _loopedOnce;
		}
	}

	private Character c;

	// Use this for initialization
	void Start () {
		c = GetComponent<Character>();
	}

	public void restart(){
		pathIndex = 0;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if(path.Count>0 && pathIndex<path.Count){
			Vector3 pos = getNextObjective();
			if( Vector3.Distance(transform.position,pos)<threshold ){
				pathIndex ++;
				if( pathIndex==path.Count && loop) {
					pathIndex = 0;
					_loopedOnce = true;
				}
			}
			c.goToPoint(pos, c.decelerateWhenDistanceIsLessThan);
			
		}
	}
	
	
	public Vector3 getNextObjective() {
		Vector3 pos;
		pos.x = path[pathIndex].x;
		pos.y = path[pathIndex].y;
		pos.z = transform.position.z;
		return pos;
	}
}
