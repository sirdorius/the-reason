using UnityEngine;
using System.Collections;

public class RandomMovement : Movement {
	public int Radius;
	public float threshold = 50;

	private Character c;
	private Vector3 randomPos;
	private Vector3 startPos;

	// Use this for initialization
	void Start () {
		c = GetComponent<Character>();
		startPos = transform.position;
		newRandomPosition();
	}

	public void newRandomPosition() {
		randomPos.x = (float)(Random.value-0.5)*Radius;
		randomPos.y = (float)(Random.value-0.5)*Radius;
		randomPos = startPos + randomPos;
	}

	// Update is called once per frame
	void FixedUpdate () {
		c.goToPoint(randomPos, 1);
		if(c.getDirection().magnitude<threshold){
			newRandomPosition();
		}
	}
}
