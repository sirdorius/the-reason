using UnityEngine;
using System.Collections;

public class LoveShieldTrigger : MonoBehaviour {
	private MouseLight theLight;
		
	void Start() {
		theLight = GameObject.Find ("TheLight").GetComponent<MouseLight>();
	}
	
	void OnTriggerEnter(Collider c) {
		var e = c.gameObject.GetComponent<Character>();
		if (e is Basil){
			theLight.explosionSize *= 2;
			((Basil)e).invulnerable = true;
		}
	}
	
	void OnTriggerExit(Collider c) {
		var e = c.gameObject.GetComponent<Character>();
		if (e is Basil){
			theLight.explosionSize /= 2;
			((Basil)e).invulnerable = false;
		}
	}
}
