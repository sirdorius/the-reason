using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrapActivator : MonoBehaviour {
	public List<GameObject> trapsControlled;
	public bool openAtStart;
	public float rechargeTime;
	
	private bool state;
	private float lastChange;
	
	void Start() {
		changeState (openAtStart);
		state = openAtStart;
		lastChange = -100;
	}
	
	private void changeState(bool open) {
		foreach(var v in trapsControlled) {
			v.GetComponent<OTSprite>().frameName = open ? "xoropen" : "xorclosed";
			var h = v.GetComponent<Hole>();
			if (!h)
				print ("Trap activator "+name+" not initialized correctly");
			h.collider.enabled = open;
		}
		lastChange = Time.time;
	}
	
	void OnTriggerEnter(Collider other) {
		foreach (var s in other.GetComponents<MonoBehaviour>())
			if (s is HazardsInteractible && ((HazardsInteractible)s).activate()) {
				if(Time.time - lastChange < rechargeTime) return;
				state = !state;
				changeState(state);
			}
	}
}
