using UnityEngine;
using System.Collections;

public class LightCollider : MonoBehaviour {
	GameObject player;
	public float range=100;
	void Start() {
		player = GameObject.Find ("Basil").gameObject;
	}
	void Update() {
		Vector2 mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector2 playerPos = player.transform.position;
		if((mPos - playerPos).magnitude < range) {
			if (Globals.gameState==Globals.GameState.LevelStart)
				Globals.changeState(Globals.GameState.Playing);
		}
	}
}
