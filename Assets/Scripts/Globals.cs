using UnityEngine;
using System.Collections;

public class Globals : MonoBehaviour {
	// TODO has to be set from main menu when starting game
	// to keep values from one scene to another
	
	public static bool SaveMode = false;
	
	public static int lives = 5;
	public static int fearCount = 0;
	
	public enum GameState {Playing=0, Paused, Dead, LevelStart, Loading};
	public static GameState gameState = GameState.LevelStart;

	public enum Powers {None=0, Explosion, Wall, Mine};
	public static Powers selectedPower = Powers.None;
	private static int aviablePowers = 0;

	private static HealthIndicator healthIndicatorSprite;
	private static Exit exit;
	public static int livesLeft = lives;
	
	public static void playerDied() {
		gameState = GameState.Dead;
		Time.timeScale = 0;
	}
	
	public static void nextPower() {
		if(aviablePowers == 0)
			return;
		if((int) selectedPower == aviablePowers)
			selectedPower = Powers.Explosion;
		else
			selectedPower++;
	}
	
	public static void previousPower() {
		if(aviablePowers == 0)
			return;
		if(selectedPower == Powers.Explosion || selectedPower == Powers.None)
			selectedPower = (Powers) aviablePowers;
		else
			selectedPower--;
	}
		
	
	public static void EnablePower(Powers power) {
		switch(power) {
		case Powers.None:
			aviablePowers = 0;
			break;
		case Powers.Explosion:
			aviablePowers = 1;
			break;
		case Powers.Wall:
			aviablePowers = 2;
			break;
		case Powers.Mine:
			aviablePowers = 3;
			break;
		}
	}
	
	public static void changeState(GameState newState) {
		var mask = GameObject.Find("PauseDarkness");
		gameState = newState;
		switch (newState) {
		case GameState.LevelStart:
			if (mask != null) {
				mask.renderer.enabled = true;
				mask.transform.position = GameObject.Find("Basil").transform.position + new Vector3(166, 88, 0);
			}
			Music.effectsPause();
			Time.timeScale = 0;
			fearCount = 0;
			break;
		case GameState.Playing:
			if (mask != null)
				mask.renderer.enabled = false;
			Music.effectsPlay();
			Time.timeScale = 1;
			break;
		case GameState.Loading:
			break;
		}
	}
	
	public static void restartLevel() {
		if(Globals.SaveMode){
			int currentLevel = PlayerPrefs.GetInt("CheckpointLevel");
			Application.LoadLevel(currentLevel);
		}
		else {
			Application.LoadLevel(Application.loadedLevel);
		}
		healthPickup(lives);
		changeState(GameState.LevelStart);
	}
	
	public static void previousLevel() {
		PlayerPrefs.SetInt("CurrentLevel", Application.loadedLevel-1);
		PlayerPrefs.Save();
		int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
		Application.LoadLevel(currentLevel);
	}
	
	public static void finishedLevel() {
		if(Globals.SaveMode){
			int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
			Application.LoadLevel(currentLevel);
		}
		else Application.LoadLevel(Application.loadedLevel+1);
		//livesLeft = lives;
		//changeState(GameState.LevelStart);
	}

	public static void healthPickup(int addlives){
		livesLeft += addlives;
		if(livesLeft >lives) livesLeft = lives;
		if (!healthIndicatorSprite)
			healthIndicatorSprite = GameObject.Find("HealthIndicator").GetComponent<HealthIndicator>();
		healthIndicatorSprite.updateLife(livesLeft);
	}

	public static void loseLife() {
		livesLeft--;
		//restart level if no more lives
		if (livesLeft <= 0){
			playerDied();
		}
		// notify the HUD element that life has decreased
		if (!healthIndicatorSprite)
			healthIndicatorSprite = GameObject.Find("HealthIndicator").GetComponent<HealthIndicator>();
		healthIndicatorSprite.updateLife(livesLeft);
	}

	public static void closeExit(){
		if (!exit){
			exit = GameObject.Find("Exit").GetComponent<Exit>();
		}
		exit.renderer.enabled = false;
		exit.collider.enabled = false;
		exit.transform.GetChild(0).renderer.enabled = true;
	}

	public static void restoreExit(){
		if (!exit){
			exit = GameObject.Find("Exit").GetComponent<Exit>();
		}
		exit.renderer.enabled = true;
		exit.collider.enabled = true;
		exit.transform.GetChild(0).renderer.enabled = false;
	}

	public static void changePower(Powers newPower) {
		selectedPower = newPower;
	}	
}
