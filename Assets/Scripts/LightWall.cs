using UnityEngine;
using System.Collections;

public class LightWall : MonoBehaviour {
	
	public float timeToLive {get;set;}
	public bool canActivateTiles {get;set;}
	private Vector3 initialScale;
	private float timeCreated = 0f;
	private OTSprite sprite;

	// Use this for initialization
	void Start () {
		sprite = GetComponent<OTSprite>();
		initialScale = sprite.transform.localScale;
		StartCoroutine(life());
	}
	
	// Update is called once per frame
	void Update () {
		timeCreated += Time.deltaTime*Time.timeScale;
		sprite.alpha = Mathf.Lerp(1f, 0.6f, timeCreated/timeToLive);
		sprite.transform.localScale = initialScale * Mathf.Lerp(1f, 0.5f, timeCreated/timeToLive);
	}
	
	IEnumerator life() {
		yield return new WaitForSeconds(timeToLive);
		transform.position = new Vector3(-5000,-5000,0);
		yield return new WaitForFixedUpdate();
		Destroy(gameObject);
	}
}
