using UnityEngine;
using System.Collections;

public class AchievementRunner : Achievement {
	public float time;
	private float timeElapsed;
	void Update() {
		if(Globals.gameState == Globals.GameState.Playing){
			timeElapsed += Time.deltaTime;
		}
	}
	public override bool resolved () {
		return timeElapsed<time;
	}
	public override string resolvedText() {
		return "Sei riuscito a completare il livello in "+timeElapsed+" secondi";
	}
}
