using UnityEngine;
using System.Collections;

public class Jealousy : Character {
	public float talkToLoveTime;
	public float travelSpeed;
	
	private PathMovement movement;
	Love love;
	
	override protected void Start() {
		base.Start ();
		GameObject.Find ("Love").GetComponent<Love>().stunTimer = talkToLoveTime;
		movement = GetComponent<PathMovement>();
		love = GameObject.Find ("Love").GetComponent<Love>();
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
	}

	void OnCollisionEnter(Collision c) {
		if (c.gameObject.name == "Love") {
			StartCoroutine(goToObjective(movement.getNextObjective(), talkToLoveTime*0.9f));
		}
	}
	
	IEnumerator goToObjective(Vector3 p, float afterSeconds) {
		if (movement.loopedOnce) {
			love.jealousyDied();
			kill ();
			yield break;
		}
		yield return new WaitForSeconds(afterSeconds);
		new OTTween(transform, 1/travelSpeed*((p-transform.position).magnitude)/10f, OTEasing.QuadOut).Tween("position", p);
		collider.enabled = false;
		yield return new WaitForSeconds(0.5f);
		collider.enabled = true;
	}
	
	
	
	public override void damage (){
		
	}
	
	public override void kill ()
	{
		// deactivate all colliders
		collider.enabled = false;
		StartCoroutine(killAnimation());
	}
	
	private IEnumerator killAnimation() {
		new OTTween(transform, 1f, OTEasing.QuadIn).Tween("localScale", transform.localScale*1.5f);
		yield return new WaitForSeconds(1f);
		new OTTween(transform, 2f, OTEasing.QuadIn).Tween("localScale", Vector3.zero);
		new OTTween(GetComponent<OTAnimatingSprite>(), 2f, OTEasing.SineIn).Tween("alpha", 0f);
		yield return new WaitForSeconds(2f);
		Destroy (gameObject);
	}
	
	public override string getName (){
		 return "Jealousy";
	}
}
