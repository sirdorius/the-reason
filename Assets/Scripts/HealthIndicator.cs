using UnityEngine;
using System.Collections;

public class HealthIndicator : MonoBehaviour {
	private static AudioClip llowPulse;
	private static AudioClip lowPulse;
	private static AudioClip highPulse;
	private static GameObject[] lives;

	void Start() {
		if(!llowPulse) llowPulse = (AudioClip) Resources.Load("Sound/Heart/LowLowPulse");
		if(!lowPulse) lowPulse = (AudioClip) Resources.Load("Sound/Heart/LowPulse");
		if(!highPulse) highPulse = (AudioClip) Resources.Load("Sound/Heart/HighPulse");

		audio.loop = true;
		audio.clip = lowPulse;

		lives = new GameObject[Globals.lives];
		Vector3 p = transform.position;
		for(int i = 0; i<lives.Length; i++){
			lives[i] = OT.CreateObject("live");
			if(i!=0) p.x += 50f;
			lives[i].transform.position = p;
		}
		updateLife(Globals.livesLeft);
	}
	
	public void updateLife(int life) {
		AudioClip clip= null;
		switch(Globals.livesLeft){
			case 5:
			case 4:
				clip = llowPulse;
				break;
			case 3:
				clip = lowPulse;
				break;
			case 2:
			case 1:
				clip = highPulse;
				break;
		}
		Music.playEffect(audio, clip, 4);
		for(int i=0; i<Globals.lives; i++){
			if(i<Globals.livesLeft){
				lives[i].SetActive(true);
				OTSprite o = lives[i].GetComponent<OTSprite>();
				o.frameIndex = life - 1;
			}
			else{
				lives[i].SetActive(false);
			}
		}
	}
}
