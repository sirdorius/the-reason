using UnityEngine;
using System.Collections;

public class PowerShield : Power {
	private Vector2 lastPosition;
	public float stoppedAfterUsage;
	public int maxWallCount;
	public bool activatesTiles;
	private int wallCount = 30;
	private Basil basil;
	private float oldBasilAccel;

	void Awake () {
		thisPower = Globals.Powers.Wall;
		basil = GameObject.Find("Basil").GetComponent<Basil>();
		oldBasilAccel = basil.acceleration;
	}

	protected override void powerStart() {
		basil.acceleration = 0;
		lastPosition = transform.position;
		powerEnable = true;
		wallCount = maxWallCount;
	}

	protected override void power() {
		if(Input.GetMouseButtonUp(0) || wallCount <= 0) {
			powerEnd();
		}
		Vector2 distance = lastPosition - (Vector2) transform.position;
		if(distance.magnitude <= 10.0f) return;
		int k = 1;
		while(Vector2.ClampMagnitude(distance, k*10.0f).sqrMagnitude < distance.sqrMagnitude) {
			wallCount--;
			GameObject wallElement = OT.CreateObject("LightWall");
			wallElement.transform.position = (Vector3) lastPosition + Vector3.ClampMagnitude(distance, k*10.0f);
			wallElement.GetComponent<LightWall>().timeToLive = maxWallCount/10 + 2.0f;
			wallElement.GetComponent<LightWall>().canActivateTiles = activatesTiles;
			// Rotate the piece of wall in the direction of basil
			//Vector3 dir = (wallElement.transform.position-basil.gameObject.transform.position);
			//wallElement.transform.Rotate(0, 0, (Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x) - 90));
			// Rotate the piece of wall in the direction of the last piece
			Vector3 dir = (new Vector2(wallElement.transform.position.x, wallElement.transform.position.y)-lastPosition);
			wallElement.transform.Rotate(0, 0, (Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x)));
			k++;
		}
		lastPosition = transform.position;
	}
	
	protected override void powerEnd() {
		base.powerEnd();
		Invoke("restoreAccel", stoppedAfterUsage);
	}
	
	private void restoreAccel() {
		basil.acceleration = oldBasilAccel;
	}
}

