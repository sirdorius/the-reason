using UnityEngine;
using System.Collections;

public class JumpToNextLevelAfter : MonoBehaviour {
	public float duration = 5f;

	// Use this for initialization
	void Start () {
		Globals.changeState(Globals.GameState.Playing);
		if (duration > 0f) {
			StartCoroutine(nextScene());
		}
	}
	
	IEnumerator nextScene(){
		yield return new WaitForSeconds(duration);
		if(Globals.SaveMode){
			PlayerPrefs.SetInt("CurrentLevel", Application.loadedLevel+1);
			PlayerPrefs.SetInt("CheckpointLevel", Application.loadedLevel+1);
			PlayerPrefs.Save();
		}
		Globals.finishedLevel();
	}
	
	/*void startPlaying() {
		Globals.changeState(Globals.GameState.Playing);
	}
	
	void waitForPlayer() {
		Globals.changeState(Globals.GameState.LevelStart);
	}*/
}
