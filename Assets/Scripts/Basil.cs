using UnityEngine;
using System.Collections;

public class Basil : Character {
	public float invulnerabilityTime;
	public bool canGetAcrossPowerWall;
		
	private float invuln = 0.0f;		// actual time of invulnerability remaining
	private float flashFreq = 0.1f;
	private GameObject theLight;	
	private bool notMoving;

	private AudioClip sigh;
	private AudioClip hurt;
	
	bool _invulnerable;
	public bool invulnerable {
		get {
			return _invulnerable;
		}
		set {
			_invulnerable = value;
		}
	}
	
	private int touchingPlatforms = 0;   // the name field
	
	public void addPlatform() {
		if (touchingPlatforms == 0)
			Physics.IgnoreLayerCollision(11,10);
		touchingPlatforms++;
	}
	
	public void subPlatform() {
		if (touchingPlatforms == 1)
			Physics.IgnoreLayerCollision(11,10, false);
		touchingPlatforms--;
	}

	void Awake () {
		sigh = (AudioClip) Resources.Load("Sound/Breath/9891__the-justin__sigh");
		hurt = (AudioClip) Resources.Load("Sound/Hurt/44428__thecheeseman__hurt1");
	}

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		theLight = GameObject.Find("TheLight");
		GetComponent<OTAnimatingSprite>().flipHorizontal = true;
		Globals.changeState(Globals.GameState.LevelStart);
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
		if (invuln > 0.0f) {
			// flash player (fa cagare flash player)
			if(Mathf.Ceil(invuln/flashFreq) % 2 == 0)
				gameObject.renderer.enabled = false;
			else
				gameObject.renderer.enabled = true;
			
			// update invuln
			invuln -= Time.deltaTime * Time.timeScale;
			if (invuln < 0.1f) {
				invuln = 0.0f;
				gameObject.renderer.enabled = true;
			}
		}
	}
	
	override protected void FixedUpdate() {
		base.FixedUpdate();
		// Attract Basil to the light
		if (!notMoving){
			Vector3 p = theLight.transform.position;
			p.z = 0;
			goToPoint(p, decelerateWhenDistanceIsLessThan);
		} else {
			if(rigidbody.velocity.x < 0)
				rigidbody.AddForce(Vector3.right*10000);
			else
				rigidbody.AddForce(Vector3.left*10000);					
		}
	}
	
	void OnTriggerExit(Collider c) {
		if (Globals.fearCount>0 && c.tag == "light") {
			FLIPTHRESHOLD = 300f;
			notMoving = true;

		}
		if(c.gameObject.layer == 15 && canGetAcrossPowerWall) // light bridge layer
			subPlatform();
	}
	
	void OnTriggerEnter(Collider c) {
		if (c.tag == "light") {
			StartCoroutine(WaitAndFree());
		}
		if(c.gameObject.layer == 15 && canGetAcrossPowerWall) // light bridge layer
			addPlatform();
	}

	public void sighAudio() {
		Music.playEffect(audio, sigh, 2);
	}

	override public void damage() {
		if (invuln == 0.0f && !_invulnerable){
			Music.playEffect(audio, hurt, 3);
			invuln = invulnerabilityTime;
			Globals.loseLife();
		}
	}
		
	override public void kill() {
		Globals.playerDied();
	}
	
	override public string getName() {
		return "Basil";
	}

	IEnumerator WaitAndFree() {
		yield return new WaitForSeconds(1.0f);
		FLIPTHRESHOLD = 85f;
		notMoving = false;
	}
}
