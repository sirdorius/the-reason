using UnityEngine;
using System.Collections;

public class FallingGround : MonoBehaviour {
	
	public bool activatedByEnemies = true;
	public float energy = 1.0f;
	private bool fall = false;
		
	// Update is called once per frame
	void Update () {
		if(fall) {
			energy -= Time.deltaTime;
		}
		if(energy <= 0) {
			GetComponent<OTAnimatingSprite>().frameName = "hole";
			gameObject.AddComponent("Hole");
			Destroy(this);
		}	
	}
	
	void OnTriggerEnter(Collider other) {
		foreach (var s in other.GetComponents<MonoBehaviour>())
			if (s is HazardsInteractible && ((HazardsInteractible)s).activate()) {
				var c = other.GetComponent<Character>();
				if (c is Enemy && !activatedByEnemies)
					return;
				fall = true;
			}
	}
}
