using UnityEngine;
using System.Collections;

public class Worthlessness : MonoBehaviour {
	public float windIntervall=5;
	public float windDuration=2;
	private float time;
	private Vector2 originSize;
	public int force=100;
	public int range=200;

	// Use this for initialization
	void Start () {
		time = 0;
		originSize = GetComponent<OTSprite>().size;
	}
	void Update () {
		time += Time.deltaTime*Time.timeScale;
		Vector3 rp = transform.position;
		rp.x += range-0.3f;
		if(time>=windIntervall){
			Vector2 s = new Vector2(range,originSize.y);
			new OTTween(GetComponent<OTSprite>(),1f,OTEasing.BounceIn).Tween("size", s);

			foreach(RaycastHit r in Physics.CapsuleCastAll(transform.position,rp,0.3f,Vector3.right)){
				if(r.collider.tag!="light" && r.collider.tag!="wall"){
						Vector3 dist = transform.position - r.collider.transform.position;
						r.collider.rigidbody.AddForce(-dist*force*Time.timeScale, ForceMode.Acceleration);
				}
			}
			if(time>windIntervall+windDuration){
				time = 0;
				new OTTween(GetComponent<OTSprite>(),1f,OTEasing.Linear).Tween("size", originSize);
			}
		}
	}
}
