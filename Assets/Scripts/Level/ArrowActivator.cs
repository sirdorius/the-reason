using UnityEngine;
using System.Collections;

public class ArrowActivator : MonoBehaviour {
	public GameObject arrowLauncher;
	public float shotDelay;
	public float arrowSpeed;
	
	private float timeSinceLastShot = 1000;
	
	void Update() {
		timeSinceLastShot += Time.deltaTime;
	}
	
	void OnTriggerStay(Collider c) {
		foreach (var s in c.GetComponents<MonoBehaviour>())
			if (s is HazardsInteractible && ((HazardsInteractible)s).activate() && timeSinceLastShot > shotDelay)
			{
				var a = OT.CreateObject("Arrows");
				a.transform.position = arrowLauncher.transform.position;
				a.transform.rotation = arrowLauncher.transform.rotation;
				float angle = (arrowLauncher.transform.rotation.eulerAngles.z-90)*Mathf.Deg2Rad;
				a.GetComponent<Arrow>().direction = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0);
				a.GetComponent<Arrow>().speed = arrowSpeed;
				timeSinceLastShot = 0;
			}
	}
}
