using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class XorController : MonoBehaviour {
	//public int totalTiles;
	public int activeTiles = 1;
	
	public List<GameObject> tiles = new List<GameObject>();
	
	public int next;

	// Use this for initialization
	void Start () {
		var i = 0;
		GameObject tile;
		
		while(true) {
			i++;
			if (tiles.Count <= activeTiles) next = tiles.Count;
			tile = GameObject.Find("Xor"+i);
			if(tile == null) break;
			tiles.Add(tile);
		}
	}
	
	public void updateSeq(GameObject oldObj, GameObject newObj) {
		tiles[tiles.IndexOf(oldObj)] = newObj;
	}
	
	public void activateNext() {
		var o = OT.CreateObject("XorClosed");
		o.transform.position = tiles[next].transform.position;
		Destroy(tiles[next]);
		tiles[next] = o;
		next = (next+1) % tiles.Count;
	}
}
