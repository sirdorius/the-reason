using UnityEngine;
using System.Collections;

public class XorClosed : MonoBehaviour {
	private float dissappearDelay = 0.1f;
	
	void OnTriggerEnter(Collider other) {
		foreach (var s in other.GetComponents<MonoBehaviour>())
			if (s is HazardsInteractible && ((HazardsInteractible)s).activate())
				GameObject.Find("Level").GetComponent<XorController>().activateNext();
	}
		
	void OnTriggerExit(Collider other) {
		foreach (var s in other.GetComponents<MonoBehaviour>())
			if (s is HazardsInteractible && ((HazardsInteractible)s).activate())
				StartCoroutine(changeTile());
	}
	
	private IEnumerator changeTile() {
		yield return new WaitForSeconds(dissappearDelay);
		var o = OT.CreateObject("XorOpen");
		o.renderer.enabled = false;
		GameObject.Find("Level").GetComponent<XorController>().updateSeq(gameObject, o);
		o.transform.position = transform.position;
		Destroy (gameObject);
		o.renderer.enabled = true;
	}
}
