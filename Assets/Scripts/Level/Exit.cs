using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour {
	public bool checkpoint = false;
	private OTSprite paper;

	void Awake() {
		if(!paper) paper = OT.CreateObject("Paper").GetComponent<OTSprite>();
	}

	public void showPaper(string text){
		new OTTween(paper,0.04f,OTEasing.ElasticOut).Tween("position", new Vector2(0,0));
		TextMesh t = paper.transform.Find("Text").GetComponent<TextMesh>();
		t.text = text;
		Time.timeScale = 0.01f;
	}

	void Update(){
		if(paper.position == Vector2.zero && enabled){
			Time.timeScale = 0;
			if(Input.GetMouseButtonDown(0)){
				new OTTween(paper.GetComponent<OTSprite>(),1f,OTEasing.ElasticOut).Tween("position", new Vector2(-1447.89f,0f));
				Time.timeScale = 1;
				Globals.finishedLevel();
			}
		}
	}

	void OnTriggerEnter(Collider c) {
		if(enabled){
			var other = c.GetComponent<Character>();
			if( other is Basil ) {
				Globals.changeState(Globals.GameState.Loading);
				if(Globals.SaveMode){
					string difficulty = PlayerPrefs.GetString("Difficulty");
					if(difficulty == "Normal" || (difficulty == "Hard" && checkpoint)){
						print("Si");
						PlayerPrefs.SetInt("CheckpointLevel", Application.loadedLevel+1);
					}
					PlayerPrefs.SetInt("CurrentLevel", Application.loadedLevel+1);
					PlayerPrefs.Save();
				}
				GameObject g = GameObject.Find("Achievements");
				if(g){
					Achievement[] acs = g.GetComponents<Achievement>();
					string text = "Congratulations\n";
					if (acs != null){
						foreach(Achievement a in acs){
							if(a.resolved()){
								text += a.resolvedText()+"\n";
							}
						}
						showPaper(text);
					}
				}
				else Globals.finishedLevel();
			}
		}
	}
}
