using UnityEngine;
using System.Collections;

public class Hole : MonoBehaviour {

	public float forceModifier = 10;
	public float noReturnPoint = 50;
	public bool fakeHole = false;
				
	void OnTriggerStay(Collider other) {
		float timeMultiplier = 1;
		if (other.tag == "damage") return;
		if (other.attachedRigidbody == null) return;
		Character c = other.GetComponent<Character>();
		if (c != null) {
			if (c.attractingHole == null) c.attractingHole = gameObject;
			if (c.attractingHole != gameObject) return;
			timeMultiplier += c.attractionDistance / transform.localScale.x * 2;
			if (timeMultiplier >= 2.5) c.stun();
		}
		Vector3 dist = transform.position - other.transform.position;
		other.attachedRigidbody.AddForce(Mathf.Pow(timeMultiplier,2)*dist.normalized*(forceModifier*100 - dist.magnitude*3), ForceMode.Acceleration);
		if(Vector3.Distance(other.transform.position, transform.position) < noReturnPoint) {
			var character = other.GetComponent<Character>();
			if (character) {
				character.gameObject.collider.enabled = false;
				character.gameObject.rigidbody.velocity = Vector3.zero;
				character.stun(1.0f);
				// TODO rotation animation
				new OTTween(character.gameObject.transform, 1.0f, OTEasing.CircIn).Tween("localScale", new Vector3(0,0,0));
				StartCoroutine(WaitAndKill(character));
			}
		}
	}
	
	void OnTriggerExit(Collider other) {
		Character c = other.GetComponent<Character>();
		if (c != null && c.attractingHole == gameObject) {
			c.attractingHole = null;
		}
	}
	
	IEnumerator WaitAndKill(Character c) {
		yield return new WaitForSeconds(1.0f);
		if (fakeHole)
			Globals.finishedLevel();
		else
			c.kill();
	}
}
