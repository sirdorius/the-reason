using UnityEngine;
using System.Collections;

public class Melancholia : MonoBehaviour {
	public float decelerator = 4;
	public bool enemyEffect = true;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "light") return;
		if(other.tag == "basil"){
			Basil b = other.GetComponent<Basil>();
			b.acceleration /= decelerator;
		}
		else if(other.tag == "enemy" && enemyEffect){
			Enemy e = other.GetComponent<Enemy>();
			e.acceleration /= decelerator;
		}
	}
	void OnTriggerExit(Collider other) {
		if (other.tag == "light") return;
		if(other.tag == "basil"){
			Basil b = other.GetComponent<Basil>();
			b.acceleration *= decelerator;
		}
		else if(other.tag == "enemy" && enemyEffect){
			Enemy e = other.GetComponent<Enemy>();
			e.acceleration *= decelerator;
		}
	}
}
