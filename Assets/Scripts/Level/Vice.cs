using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Vice : MonoBehaviour {

	public float forceModifier = 10;
	public float tangentForceModifier = 0;
	private float noReturnPoint;
	public float rotationSpeed;
	private List<int> ID = new List<int>();
	private List<Vector3> pos = new List<Vector3>();
	
	void Start() {
		noReturnPoint = transform.localScale.x*0.2f;
		if(noReturnPoint < 30) noReturnPoint = 30;
	}
	
	/*void Update() {
		if( rotationSpeed != 0) {
			float dt = Time.deltaTime*Time.timeScale;
			transform.Rotate(0,0,dt*-rotationSpeed);
		}
	}*/
	
	void OnTriggerEnter(Collider other) {
		Character c = other.gameObject.GetComponent<Character>();
		if((c is Basil) || (c is Anger)) {
			ID.Add(other.gameObject.GetInstanceID());
			pos.Add(other.gameObject.transform.position);
		}
	}
	
	void OnTriggerExit(Collider other) {
		if(ID.Contains(other.gameObject.GetInstanceID())) {
			int index = ID.IndexOf(other.gameObject.GetInstanceID());
			pos.RemoveAt(index);
			ID.Remove(other.gameObject.GetInstanceID());
		}
	}
	
	void OnTriggerStay(Collider other) {
		if (other.tag == "damage") return;
		Vector3 dist = transform.position - other.transform.position;
		other.attachedRigidbody.AddForce(dist.normalized*(forceModifier*100 - dist.magnitude*3), ForceMode.Acceleration);
		Vector3 tan = new Vector3(dist.normalized.y, -dist.normalized.x, 0);
		other.attachedRigidbody.AddForce(tan*(tangentForceModifier*100 - dist.magnitude*3), ForceMode.Acceleration);
		if(Vector3.Distance(other.transform.position, transform.position) < noReturnPoint) {
			other.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
			Character character = other.GetComponent<Character>();
			if (character) {
				if((character is Basil) || (character is Anger)) {
					character.damage();
					int index = ID.IndexOf(other.gameObject.GetInstanceID());
					Vector3 oldPos = pos[index];
					pos.RemoveAt(index);
					ID.Remove(other.gameObject.GetInstanceID());
					if(character) {
						character.stun(1.0f);
						character.rigidbody.velocity = Vector3.zero;
						other.gameObject.transform.position = oldPos + (oldPos - transform.position)*0.3f;
					}
				}
				else {
					character.stun(2.0f);
					character.kill();
				}
			}
		}
	}
}