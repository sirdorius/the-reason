using UnityEngine;
using System.Collections;

public class Quote : MonoBehaviour {
	private OTSprite paper;
	public string text;
	public bool destroyOnRead = true;
	private bool readMe = false;

	void Awake () {
		if(!paper) paper = OT.CreateObject("Paper").GetComponent<OTSprite>();
		for(int i = 0; i<text.Length/30;i++){
				int index = text.IndexOf(" ",29*(i+1));
				if(index!=-1) text = text.Insert(index,"\n");
		}
	}
	void OnTriggerEnter(Collider c) {
		if(c.tag=="basil"){
			new OTTween(paper,0.04f,OTEasing.ElasticOut).Tween("position", new Vector2(0,0));
			TextMesh t = paper.transform.Find("Text").GetComponent<TextMesh>();
			t.text = text;
			readMe = true;
			Time.timeScale = 0.01f;
		}
	}
	void Update() {
		if(paper.position == Vector2.zero)
			Time.timeScale = 0;
		if(Input.GetMouseButtonDown(0) && readMe){
			new OTTween(paper.GetComponent<OTSprite>(),1f,OTEasing.ElasticOut).Tween("position", new Vector2(-1447.89f,0f));
			Time.timeScale = 1;
			if(destroyOnRead && readMe)
				Destroy(gameObject);
			Globals.changeState(Globals.GameState.LevelStart);
		}
	}
}
