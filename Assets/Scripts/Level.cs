using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {
	
	void Awake() {
	}
	
	public void spawnRequest(Object type, Vector3 pos, float delay) {
		//print ("spawned "+type.name);
		StartCoroutine(spawnRequestExecutor(type, pos, delay));
	}
	
	private IEnumerator spawnRequestExecutor(Object type, Vector3 pos, float delay) {
		GameObject o = (GameObject)Instantiate(type);
		GameObject c = GameObject.Find ("Enemies");
		o.transform.parent = c.transform;
		o.SetActive(false);
		yield return new WaitForSeconds(delay);
		o.transform.position = pos;
		o.SetActive(true);
	}
}
