using UnityEngine;
using System.Collections;

public class AchievementNoPower : Achievement {
	public bool onlyThis = false;
	public Globals.Powers power = Globals.Powers.None;
	private bool res = true;
	private bool happen = false;

	// Update is called once per frame
	void Update () {
		if(!happen){
			if(power == Globals.Powers.None && Input.GetMouseButtonDown(0)){
				happen = true;
				res = false;
			}
			else if(!onlyThis){
				if(power == Globals.selectedPower && Input.GetMouseButtonDown(0)){
					happen = true;
					res = false;
				}
			}
		}
	}
	public override bool resolved() {
		return res;
	}
	public override string resolvedText(){
		return "Non hai utilizzato il potere...";
	}
}
