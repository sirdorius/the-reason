using UnityEngine;
using System.Collections;

public class MineExplosion : MonoBehaviour {
	public float time = 1f;
	
	public float range {get;set;}
	
	void Start() {
		new OTTween(transform, time/2f, OTEasing.CircOut).Tween("localScale", new Vector3(range/2,range/2,1));
		Invoke("shrink", time/2f);
	}
	
	void shrink() {
		new OTTween(transform, time/2f, OTEasing.QuadIn).Tween("localScale", new Vector3(0,0,1));
		new OTTween(GetComponent<OTSprite>(), time/2f, OTEasing.Linear).Tween("alpha", 0.2f);
		Destroy(gameObject,	time/2f);
	}

	void OnTriggerEnter(Collider c) {
		if (c.tag=="enemy") {
			c.gameObject.GetComponent<Enemy>().stun();
		}
	}
}
