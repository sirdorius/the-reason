using UnityEngine;
using System.Collections;

public class Nail : MonoBehaviour {
	public float rotationSpeed;
	
	private Vector3 direction;
	public GameObject createdBy {set;get;}
	public float speed {set;get;}
	public bool damageSameType {set;get;}
	
	private Transform nailsprite;

	// Use this for initialization
	void Start () {
		rigidbody.AddForce(direction*speed);
		foreach (Transform child in transform)
			nailsprite = child;
	}

	
	public void setDirection(Vector3 d) {
		direction = d;
	}

	// Update is called once per frame
	void Update () {
		float dt = Time.deltaTime*Time.timeScale;
		nailsprite.transform.Rotate(0,0,dt*rotationSpeed*100);
	}
	
	void OnTriggerEnter(Collider c) {
		if (c.collider.gameObject != createdBy) {
			Character hitChar = c.collider.GetComponent<Character>();
			if (createdBy != null && !damageSameType && hitChar is Anxiety)
				return;
			if (hitChar is Character)
				hitChar.damage();
			Destroy(gameObject);
		}
	}
}
